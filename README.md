# Docker Getting Started - Pluralsight #

## Info ##

There have been some major changes to Docker that were introduced with Docker 1.12, and this course, Getting Started with Docker, will help get you up to speed. You'll start with installing Docker on the most common development and production platforms - Windows and Mac laptops, Windows Server 2016, and Linux. Next, you'll get to see some fundamental concepts of containers and images, including how to perform common management tasks. You'll also spend a good deal of time covering all the new stuff introduced with Docker 1.12, including Swarm mode, services, scaling, rolling updates, stacks, and distributed application bundles. After this course, you'll be up and running with some of the game-changing improvements announced with Docker 1.12 and have a solid understanding of the fundamentals of Docker.

## Section 1 ##
- Docker Install Info:
    - Allows you to run Linux or Windows Containers (but not at the sametime). 
    - Windows 10/2016+ and 64-Bit
    - Linux Containers run inside a Hyper-V VM (MobyLinux)
    - Be sure to install Hyper-V (installer will handle this for you) - requires a reboot.
    - docker version

    ``` powershell
    > docker version
    Client:
    Version:      18.03.1-ce
    API version:  1.37
    Go version:   go1.9.5
    Git commit:   9ee9f40
    Built:        Thu Apr 26 07:12:48 2018
    OS/Arch:      windows/amd64
    Experimental: false
    Orchestrator: swarm

    Server:
    Engine:
    Version:      18.03.1-ce
    API version:  1.37 (minimum version 1.24)
    Go version:   go1.9.5
    Git commit:   9ee9f40
    Built:        Thu Apr 26 07:21:42 2018
    OS/Arch:      windows/amd64
    Experimental: false
    ```

## Section 2 ##

Run our first container from Docker Hub/Store:

```cli
# deploy container
docker run hello-world

# Get Details
docker ps  # List containers
docker ps -a # List current and past containers

# Info
docker info
```

A little bit more interesting:

```cli
docker pull ubuntu:latest

docker pull ubuntu:14.04

docker run -it --name testMe --port 80:8080 /bin/bash
```


## Section 3 ##

Configure Bash for Windows (WSL) to run Docker. Install docker engine on Windows 10 first.

```bash
# Setup local install vars
export PATH="$HOME/bin:$HOME/.local/bin:$PATH"
export PATH="$PATH:/mnt/c/Program\ Files/Docker/Docker/resources/bin"
alias docker=docker.exe
alias docker-compose=docker-compose.exe

# Install packages to allow apt to use a repository over HTTPS
$ sudo apt-get install apt-transport-https ca-certificates curl software-properties-common
# Add Docker's official GPG key
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
# Set up the repository
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
# Update source lists
sudo apt-get update
# Install Docker
sudo apt-get install docker-ce
```


